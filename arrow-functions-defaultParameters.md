### Parametros por defecto

Por lo general cuando se declara una función los parametros por defecto suelen ser undefined.

##### parametro undefined
```javascript
const sum = (a,b) => a+b

sum(5) // NaN
```

Como solo especificamos el primer argumento, el parametro b por defecto es undefined; al sumar "5+ ``Undefined`` " el resultado es ``NaN`` 

Ahora vemos el caso estableciendo un valor por defecto al parámetro

##### parametro undefined
```javascript
const sum = (a,b=4) => a+b

sum(5) // 9
```

