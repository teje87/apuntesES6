### Destructuring 

El destructuring es una expression que nos permite extraer data de arrays, objetos, maps y sets.

#### Destructuring en objetos

Nos permite extraer multiples propiedades de  objetos de una sola vez.

Tenemos un objeto con unas propiedades y queremos asignar estas propiedades a otras variables que se encuentran en el ambito global.

```javascript
    const persona = {
        nombre: "paco",
        edad: 35,
        nacionalidad: "Senegales",
        redesSociales: {
            twitter: true,
            facebook:false
        }
    }


    const {nombre, edad, nacionalidad} = persona

    console.log(nombre) // paco

```

Si quisiesemos llegar a niveles mas profundos del objeto, tendriamos que realizar el destructuring a el nivel que queremos acceder. 

```javascript

    const persona = {
        nombre: "paco",
        edad: 35,
        nacionalidad: "Senegales",
        redesSociales: {
            twitter: true,
            facebook:false
        }
    }

    const {twitter, facebook} = persona.redesSociales 

    console.log(twitter) //true
    console.log(facebook) //false

```

#### Destructuring en arrays

Al igual que podemos realizar el destructuring en objetos tambien lo podemos realizar en arrays. A continuación un ejemplo de malas practicas y uno de buenas practicas con el destructuring.

##### ejemplo malas practicas 👎 💀 ⚠️
```javascript
    const persona = ["Mikel", "Tejero","30"]

    const nombre = persona[0] 
    const apellido = persona[1]
    const edad = persona[2]

    console.log(nombre) // "Mikel"
    console.log(apellido) // "Tejero"
    console.log(edad) // 30
```

##### ejemplo buenas practicas 👍 💯 🆒

```javascript
    const [nombre,apellido,edad] = persona

    console.log(nombre) // "Mikel"
    console.log(apellido) // "Tejero"
    console.log(edad) // 30
```

### Casos de uso interensates

#### Destructuring de un string con comas entre elementos

En ocasiones en los programas es posible que recibamos datos en formato string y estos vengan separados por comas. En el caso de que queramos pasar estos datos a un array lo haremos de la siguiente manera.

```javascript
const data = "73,30,176"

const [peso, edad, altura] = data.split(",")

console.log(peso) // 73
console.log(edad) // 30
console.log(altura) // 176
```

#### Destructuring con ...rest

Mas adelante veremos en profundidad el uso de rest y spread, tan solo pondre un ejemplo en el que se entiende facilmente.

Supongamos que tenemos un array de personas queremos saber el cargo de cada uno. Queremos asignar el puesto de trabajo a cada persona , pero solo queremos establecer que uno de los trabajadores es jefe y el resto es peon. 

```javascript
    const personas = ["Patxi","Paco","Francisco","Juan","Manuel"]


    const [jefe, ...peon] = personas

    console.log(jefe) // Patxi
    console.log(peon) // ["Paco","Francisco","Juan","Manuel"]

```

Como vemos asignamos jefe a "Patxi" y el resto les asignamos peon utilizando rest ``...peon``.


#### Intercambiando valores entre variables

Supongamos que vamos a hacer un trueque. Tenemos dos sujetos y cada uno de ellos tiene "algo" y necesitamos intercambiar lo que tiene de uno al otro.

```javascript
    
    let marco = "mono";
    let willy = "perro";
    [marco, willy] = [willy, marco]

    console.log(marco) // perro
    console.log(willy) //mono


```

#### Destructuring de una función que devuelve  un objeto

Supongamos que tenemos una cuenta, con varias cuentas bancarias asociadas y queremos crear una función que sume la misma cantidad de dinero a cada una de las cuentas asociadas.

```javascript

    function addEnTodas(cantidad){
        const cuentas = {
            principal: 20 + cantidad,
            secundaria: 53 + cantidad,
            externa: 24 + cantidad
            }

            return cuentas;
    }

    const {principal, externa} = addEnTodas(5)

    console.log(principal) // 25
    console.log(externa) //29


```
#### Destructuring y argumentos

Puede resultar util pasar los argumentos haciendo destructuring de la siguiente manera.

```javascript
function calculaCoste({ coste, tasas = 0.21, propina = 0.1}){
    return coste + coste * tasas + coste * propina
}

calculaCoste({coste: 100, propina:0.20})

```

De esta manera le puedes pasar el parametro directamente sin importar el orden en el que se pasaen los valores.


