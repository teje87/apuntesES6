### Mejoras en object literals

Pongamos el caso que tenemos una cantidad de variables y queremos que estas variables pasen a ser propiedades de un objeto.

```javascript
    const primero = "Marco"
    const segundo = "Pedro" 
    const tercero = "Antonio"
    const cuarto = "Juan"

    const integrantes = {
        primero,
        segundo
        tercero,
        cuarto
    }

    console.log(integrantes)
    // {primero: "Marco", segundo: "Pedro", tercero: "Antonio", cuarto: "Juan"}
```

De esta manera no tenemos que declarar el key/value, quedando así un código mas reducido. Con los metodos ocurre algo similar como podemos apreciar en el siguiente ejemplo.

```javascript
    const jugador = {
        saltar(){
            console.log("jugador saltando")
        },
        correr(){
            console.log("jugador corriendo")
        },
        pasar(){
            console.log("balon pasado")
        }
    }

    jugador.saltar()

    // jugador saltando
```

Por ultimo podemos generar el key de las propiedades dinamicamente. 


```javascript

const key = "elemento"

const objeto = {

    [key] : "titanio"

}

console.log(objeto.elemento) // "titanio"  

```

```javascript
    const frutas = ["manzanas","limones","peras"]
    const precio = [1.50,1,42,1.55]

    const listaDePrecios = {
        [frutas.shift()] = precio.shift(),
        [frutas.shift()] = precio.shift(),
        [frutas.shift()] = precio.shift()
    }

    console.log(listaDePrecios)
    //{manzanas: 1.5, limones: 1, peras: 42}
```