### Spread y Rest

ES6 introduce el nuevo operador spread ``...`` o rest. Será spread o rest en funcion de donde esté declarado.

Cuando está declarado en un array o un iterable actua como spread. Va a tomar cada item del iterable y lo va a aplicar en el elemento o array en el que se encuentra. Cuando lo pasamos como paramentro a una función es considerardo rest. Pasa cada elemento del array como parametro.

```javascript

const elementos = ["motor","correa","engranaje","alimentación"]

function logElements(...args){
    console.log(args)
}

logElements(...elementos)

// ["motor","correa","engranaje","alimentación"]
```

Ya vimos que podiamos utilizar el keyword ``arguments`` para utilizar todos los argumentos pasados con spread. Evitando utilizar arguments y declarando ``...args`` en el ejemplo anterior como argumento de la funcion hemos conseguido que el resultado sacado por consolar sea de prototipo array directamente, evitando así tener que hacer una conversion del prototypo arguments que es "array-ish".  

Spread también puede resultar muy util para concatenar arrays, evitando así tener que utilizar ningun metodo.

##### Ejemplo spread concatenando arrays

```javascript
    const colores = ["rojo","verde","amarillo","azule"]
    const otrosColores = ["beige","naranja","rosa"]

    const todosLosColores = [...colores, ...otrosColores]

    console.log(todosLosColores)

    // ["rojo", "verde", "amarillo", "azule", "beige", "naranja", "rosa"]
```

```javascript 
    const val = [1,2,3,4]

    const anotherVal = [6,7,8,9]

    const oneToNine = [...val, 5, ...anotherVal]

    console.log(oneToNine) // [1,2,3,4,5,6,7,8,9]
```

##### Ejemplos rest pasando paramentros

```javascript
    const jugadores = ["Fulano","Megano","Juano","Cristiano"]

    const fichajes = ["Pepe","Paco"]

    jugadores.push(...fichajes)

    console.log(jugadores) 
    //["Fulano","Megano","Juano","Cristiano","Pepe","Paco"]
```

```javascript
    const porteros = ["Willy","Pili"]

    function habloDePorteros(){
        console.log(`El portero de mi equipo es ${portero1} y el de el ${portero2}`)
    }

    habloDePorteros(...porteros)

    //El portero de mi equipo es Willy y el de él Pili

```

En este último ejemplo hay que tener en cuenta que los parámetros son pasados con spread en el orden que se encuentren en el array.
