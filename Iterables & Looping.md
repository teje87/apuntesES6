### Iterables y bucles

#### ``for...off``

El bucle ``for off`` es un nuevo modo de hacer un bucle sobre un iterable en ES6.

Previamente ya existian metodos para recorrer iterables pero veamos por qué ``for off`` ofrece ventajas respecto a otros metodos.

##### recorrer con un for

```javascript
    const cara = ["ojos","pelo","boca","nariz"]

    for(let i = 0 ; i < cara.length;i++){
        
        console.log(cara[i]) 
        
    }

    //ojos
    //pelo
    //boca
    //nariz
```

Aparentemente no lleva ningun problema realizarlo de esta manera pero la syntaxis es poco semantica y en programas grandes puede llevar trabajo comprender muchos bloques de este estilo.Por otra parte también puede llevar a declarar el index con var y generar problemas con los scopes.


##### recorrer con  .forEach()

```javascript
    
    const cara = ["ojos","pelo","boca","nariz"]

    cara.forEach((parte)=>{
        console.log(parte)
    })
```
De esta manera conseguimos el mismo resultado y es mas legible que con ``for`` pero tenemos el inconveniente de que si queremos forzar la salida en un momento dado del bucle no es posible.

##### recorrer con for ... in 

```javascript
    const cara = ["ojos","pelo","boca","nariz"]

    for(parte index cara){
        console.log(parte[index])
    }
```
Esta opción aparentemente tambien es valida, aunque no tenga mucho sentido utilizarla teniendo mejores alternativas. Ademas conlleva un problema, si utilizamos alguna libreria que modifica el prototipo array, al iterar sobre nuestro array definido, tambien iterara sobre los metodos o propiedades declarados en el prototipo Array por la libreria.

##### recorrer con for ... of

Veamos ahora a ver ahora como usamos el for ... of. Podremos utilizarlo para iterar sobre cualquier iterable excepto objetos.

```javascript
    const partes = ["ojos","pelo","boca","nariz"]

    for (const parte of partes){
    
        console.log(parte)
    
    }

    //ojos
    //pelo
    //boca
    //nariz
    
```

Como vemos el código es muy legible de esta manera y tiene todas las ventajas de los metodos anteriores.

##### ``for...of`` y el iterador

La unica pega que podemos encontrar en el ``for...of`` es no poseer el iterador (index). Utilizando el metodo ``.entries()`` sobre el array, por cada vez que corre una posición en el ciclo ``for...of`` devuelve el index y el valor, por lo tanto si hacemos un destructuring podemos obtener el iterador y el valor con ``for...of``.

```javascript

    const partes = ["ojos","pelo","boca","nariz"]

    for(const [i,parte] of partes.entries()){
        console.log(i,parte)
    }

    //0 "ojos"
    //1 "pelo"
    //2 "boca"
    //3 "nariz"
```

#### for...of con ``arguments`` 

En ocasiones queremos ejecutar una función a la que queremos pasarle una cantidad de argumentos que no es definida, no sabemos la cantidad de argumentos que se le van a pasar.

Si invocamos ``arguments`` dentro de una función hara referencia a todos los argumentos pasados a la función.

```javascript
    
    function logValores(){
        console.log(arguments[1])
    }

    logValores(2,3,4) // 3

```


```javascript
    
    function logValores(){
        console.log(arguments)
    }

    logValores(2,3,4) // [2,3,4]

```


```javascript
    
    function logValores(){
        for (const argument of arguments){
            console.log(argument)
        }
        
    }

    logValores(2,3,4) 

    //2
    //3
    //4

```

#### ``for...of`` con Strings

Podemos iterar un string caracter por caracter de la siguiente manera con la palabra reservada ``char``.

```javascript
    const name = 'Mikel';
    
    for (const char of name){
        console.log(char);
    }

    //M
    //i
    //k
    //e
    //l
```

#### ``for...of`` con NodeLists y HTMLCollections

Podemos recorrer un array de elementos HTML 

```javascript
    const ps = document.querySelectorAll('p');
    
    for (const paragraph of ps) {
        console.log(paragraph);
    }

    // Mostrara todo los elementos <p></p> del documento HTML
```

#### ``for...of`` y objetos

Al igual que utilizamos ``.entries()`` previamente podemos utilizarlo con objetos de la siguiente manera, de este modo podremos iterar objetos indirectamente.

```javascript
    const partes = {
        superior:"cabeza",
        medio: "tronco",
        inferior: "piernas"
    }

    for(const [prop,value] of Object.entries(partes)){
        console.log(prop,value)
    }
    //superior cabeza
    //medio tronco
    //inferior piernas

```