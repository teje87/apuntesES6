var-let-const
======

Es necesario aclarar las diferencias entre las distintas maneras de declarar las variables puesto que no actuan de la misma manera a nivel de **scope**.

El **scope** es el alcance que tienen nuestras variables.
___
### var

Cuando creamos una variable con **var**, la variable que creamos puede ser **reasignable**.

```javascript
var s = "sisisi";
console.log(s); // "sisisi"

s = "nonono"
console.log(s); // "nonono"
```
 
También permite declarar la misma variable varias veces sin generar ningun tipo de error, lo cual puede generarnos problemas en nuestro programas si declarasemos dos variables con el mismo nombre.

 ```javascript
var s = "sisisi";
console.log(s); // "sisisi"

var s = "nonono"
console.log(s); // "nonono"
```
### var y function scope

Las funciones tienen su propio scope ( **function scope** ), podemos ver en este caso como las var declaradas dentro de la función quedan limitadas a su scope.

```javascript 
function foo(){
    var pepe = "Yo soy er pepe"
    console.log(pepe)
}

foo(); // "Yo soy er pepe"

console.log(pepe) // !!ERROR
```

Si la variable fuese declarada fuera del function scope

```javascript 
var pepe = "Yo soy er pepe"

function foo(){
    console.log(pepe)
}

foo(); // "Yo soy er pepe"

console.log(pepe) // "Yo soy er pepe"
```

### var,let y const en block scope
Es importante diferenciar y no confundirse con el **block scope** en el que las var declaradas dentro de un bloque tienen acceso global.

```javascript
var dinerillos = 15;

if(dinerillos > 10){
    var aSacoDinerillos = dinerillos * 2
}

console.log(aSacoDinerillos) //30 
```
Los problemas surgen cuando necesitamos que la variable que hemos creado solo abarque el **block scope** y no tengamos acceso global a esta. Es aquí cuando entra en juego **let**.

```javascript
var dinerillos = 15;

if(dinerillos > 10){
    let aSacoDinerillos = dinerillos * 2
}

console.log(aSacoDinerillos) // !! ERROR 
```
El mismo ejemplo con la delaracion de "*aSacoDinerillos*" con **let** provocará un error ya que al declarar **let** dentro del **bloque if** su scope queda delimitado al bloque. **Const** actua de la misma manera que **let** a nivel de scope pero tiene ciertas particularidades que explicaremos mas adelante.

## let y const

Ya hemos dicho previamente que existian ciertas particularidades que hacian distintas a let y const. Utilizaremos const cuando queramos que lo declarado no sea **reasignable**. 

```javascript
let miNombre = "Mikel"
miNombre = "Manuel"
console.log(miNombre) // "Manuel"
_________________________
const miNombre = "Mikel"
miNombre = "Manuel" <------------ !! ERROR
console.log(miNombre)
```

Es importante diferenciar entre **reasignable** y **mutable**. Las declaraciones de const pueden mutar como veremos en el siguiente ejemplo.

```javascript
const yo = {nombre: "Mike", apellido:"Tyson", edad:"19"};
yo.nombre= "Paco";
console.log(yo); // {nombre:"Paco",apellido:"Tyson",edad:"19"}
```
La estructura del objeto no ha sido reasignada, simplemente hemos mutado una de las propiedades. Este tipo de conceptos es muy importante entenderlos bien e interiorizarlos ya que cuando estemos trabajando en programas mas grandes puede generar muchas confusiones.

Si queremos que el objeto no sea mutable podemos usar ``Object.freeze()``.De esta manera no podremos ni añadir,ni eliminar, ni modificar las propiedades del objecto.

## IIFE (inmediatly invoked function expression)

Antes de ES6 se utiliazban las **IIFE** para poder declarar variables sin que provocasen conflicto con las globales, ya que todavia no existian los **let**.

Se trata de funciones anonimas que se auto invocan, de esta manera los var declarados en el IIFE tendran function scope.

```javascript
(function () { 
    var x = 10;
})();
console.log(x);  // Reference Error: x is not defined
```

Una situación muy habitual es declarar un **for** loop con var, la cual podria entrar en conflicto con una variable global con el mismo nombre.

```javascript
    var i = 40; 
    console.log(i); //40

    for(var i = 0; i <= 30; i++){
        console.log(i) // log del 0 al 29
    }

    console.log(i); // 30
```
Declarandolo con una IIFE quedaría resuelto y no reasignaria la "i" global. Pero incluso declarandolo con una IIFE todavía queda un caso que nos podría casuar problemas. En el **ejemplo dos** la "i" del for loop no está declarada y por consiguiente el interpreter de javascript la declara globalmente, provocando una reasignación de la "i" declarada globalmente.

##### ejemplo1

```javascript
    var i = 40; 
    console.log(i); //40
    (function estoEsUnaIIFE(){
        for(var i = 0; i <= 30; i++){
        console.log(i) // log del 0 al 29
        }
    })();
    

    console.log(i); // 40
```
##### ejemplo2

```javascript
    var i = 40; 
    console.log(i); //40
    (function estoEsUnaIIFE(){
        for(i = 0; i <= 30; i++){
        console.log(i) // log del 0 al 29
        }
    })();
    

    console.log(i); // 30
```

Con la llegada de **let** despejamos los dolores de cabeza y declararemos con let en nuestros **for** loops para no tener problemas.

### Temporal Dead Zone

Cuando accedemos a una var antes de que esta este declarada provoca ``undefined``, sin embargo si aplicamos de la misma manera para **let** y **const** lanza un ``ReferenceError``. 

El **hoisting** es el proceso de mover todas las declaraciones de variables a la parte superior de la función o del entorno, es por eso que cuando hay un acceso a una variable sin que esta se haya declarando previamente da undefined. 

Con ES6 y la llegada de **let** y **const** entre el hoisting y el **binding** en el que se asigna un valor a la variable existe la **Temporal Dead Zone** que es la que provoca el error en vez de la asignación undefined con los **var**.

#### ejemplo 

```javascript
var x = 1; // Inicializa x
console.log(x + " " + y); // '1 undefined'
var y = 2; Inicializa y
```

#### interpretación

```javascript
var x = 1; // Inicializa x
var y;// Se elevo la declaración
console.log(x + " " + y); // '1 undefined' 
y = 2; Inicializa y
```

### ¿Que uso debemos usar let, var o const ?

Distintios expertos tienen su opinión particular, la mia es que sabiendo como funciona las utilices con cabeza, aún así te dejo un posible patrón a seguir.

1. Usa ``const``por norma general si no tienes que reasignar los valores.
2. Usa let si es necesario reasignar el valor en algún momento.
3. Si usas ES6 no deberías usar var











 
