### Nuevos metodos de strings

ES6 trae cuatro nuevos metodos para minimizar el uso de las expresiones regulares.

### ``.startsWith()``  y ``.endsWith()`` 

Explicaremos como se utiliza ambos metodos con unos ejemplos.

Pongamonos en la situación que clasificamos los productos en funcion a una referencia que posee cada producto. Los productos fabricados en plastico comenzaran con las letras "PLS" y los fabricados en aluminio con "ALM". En este caso queremos detectar cuales de los productos son de plástico.


```javascript
    const productos = ["PLS1231231234", "PLS4343666","ALM166765"] 

    productos.map((item)=>{
        return item.startsWith("PLS") 
    })

    //[true,true,false]
```

Si la refencia a el material no estuviese en los primeros caracteres tambien podríamos indicar la posición desde la que debería comenzar a checkear.

```javascript
    const producto = "123PLS123455"

    producto.startsWith("PLS",3)

    //True 
```

Tambien podemos comprobar si el string tiene la terminación que buscamos utilizando el metodo ``endsWith()``.

```javascript
    const producto = "1424124PLS"

    producto.endsWith("PLS")

    //True
```

También podemos indicar en el segundo argumento donde queremos que acabe el string.

```javascript
    const producto = "1412424124PLS12412"

    producto.endsWith("PLS",13)

    //True
```

Diciendole que queremos que acabe en el caracter "13" obtenemos el string "1412424124PLS" con lo cual la terminación es "PLS".

### ``.includes()``

Con el metodo ``includes()`` podemos comprobar si la cadena simplemente contiene el string que le pasemos, sin indicar si es en el comienzo o en el final a diferencia de los anteriores metodos.

```javascript
    const producto = "1421224PLS4234234"

    producto.includes("PLS")

    // True
```

### ``.repeat()``

El metodo repeat nos permite repetir x cantidad de veces el string

```javascript
    "Hola".repeat(5)

    //HolaHolaHolaHolaHola
```

De primeras quizás no le encontremos utilidad. Un posible caso en el que puede resultar util es para indentar mensajes que vamos a mostrar por consola.


##### ejemplo extraido de el blog de Wes Bos

```javascript
cons comida= {
    tipo: "carne",
    color: "rojo",
    cantidad: "poca" 
}


leftPad = function(str, length = 10){
    return `${' '.repeat(length)}${str}`;
}

console.log(leftPad(comida.tipo));   // '          carne'
console.log(leftPad(comida.color));  // '          rojo'
console.log(leftPad(comida.cantidad)); // '        poca'

```
