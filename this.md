### Conceptos importantes: Context vs. Scope

Antes de entrar de lleno a comprender el uso de ``this`` es importante diferenciar *scope* de *contexto* ya que a menudo se usan de manera incorrecta.

Cada función invocada  posee ambas asociadas a esta. El scope es basado en la función mientras que el contexto se basa en el objecto. Del scope ya hablamos previamente. Vamos con el contexto.

Cuando una función es invocada se genera un contexto de ejecución; el valor de ``this`` será el contexto de ejecución que hace referencia al objeto en el que se está ejecutando la función.  





### this

El uso de ``this`` suele resultar bastante confuso y si no lo comprendemos podemos hacer un uso erroneo de el.

A contuación vemos unos ejemplos expuestos en los libros de YDKJS para entenderlo.

##### ejemplo de uso

```javascript 

function identify() {
	return this.name.toUpperCase();
}

function speak() {
	var greeting = "Hello, I'm " + identify.call( this );
	console.log( greeting );
}

var me = {
	name: "Kyle"
};

var you = {
	name: "Reader"
};

identify.call( me ); // KYLE
identify.call( you ); // READER

speak.call( me ); // Hello, I'm KYLE
speak.call( you ); // Hello, I'm READER

``` 
Por el momento no es necesario entender el funcionamiento linea a linea, veamos de un vistazo que es lo que pasa.

El código anterior permite reusar las funciones ``speak()`` y ``identify()`` en diferentes contextos. En este caso en el objeto "me" y en el objeto "you".De esta manera no declaramos dichas en funciones en cada objeto.

También lo podíamos haber declarado de la siguiente manera sin utilizar el keyword ``this``

##### ejemplo pasando el contexto a las funciones

```javascript
function identify(context) {
	return context.name.toUpperCase();
}

function speak(context) {
	var greeting = "Hello, I'm " + identify( context );
	console.log( greeting );
}

var me = {
	name: "Kyle"
};

var you = {
	name: "Reader"
};

identify( you ); // READER
speak( me ); // Hello, I'm KYLE
``` 

Puede paracer que de esta manera es mas sencilla, pero con el uso de ``this`` además de quedar mas elegante nuestro código será mucho mas escalable. Mas adelante veremos lo util que resulta poder asignar una colección de funciones a un contexto.

### Confusiones

Es muy común pensar erroneamente que el ``this`` hace referencia a la propia función en la que se declara. 

##### <spanb style="color:red"> ejemplo erroneo ( intentar apuntar this a una función)  :-1:  ❗️️  💀 </span>

```javascript 
function foo(num) {
	console.log( "foo: " + num );

	// keep track of how many times `foo` is called
	this.count++;
}

foo.count = 0;

var i;

for (i=0; i<10; i++) {
	if (i > 5) {
		foo( i );
	}
}
// foo: 6
// foo: 7
// foo: 8
// foo: 9

// how many times was `foo` called?
console.log( foo.count ); // 0 -- WTF?

``` 

En este caso la confusión ha sido pensar que el contexto de ``this.count`` es la función.

La función ``foo()`` se encuentra en el contexto global, por lo tanto si se estaría ejecutando en un navegador haría referencia al objeto ``window``.

Por lo tanto ``foo.count`` sigue siendo 0 ya que se estaba intentando incrementar ``window.foo`` .

Por lo tanto si quisiesemos que el ``this `` apuntase a el contexto de la función , lo hariamos de la siguiente manera.

##### <span style="color:green"> ejemplo anterior modificando donde apunta ``this`` :+1: 🆒 </span>

```javascript
	
function foo(num) {

	console.log( "foo: " + num );

	// keep track of how many times `foo` is called
	// Note: `this` IS actually `foo` now, based on
	// how `foo` is called (see below)
	this.count++;
}

foo.count = 0;

var i;

for (i=0; i<10; i++) {
	if (i > 5) {
		// using `call(..)`, we ensure the `this`
		// points at the function object (`foo`) itself
		foo.call( foo, i ); 
	}
}
// foo: 6
// foo: 7
// foo: 8
// foo: 9

// how many times was `foo` called?
console.log( foo.count ); // 4
```

##### <spanb style="color:red"> ejemplo erroneo ( intentar puentear scopes)  :-1:  ❗️️  💀 </span>

```javascript
function foo() {
	var a = 2;
	this.bar();
}

function bar() {
	console.log( this.a );
}

foo(); //undefined
```
En el código siguiente hay varios errores, al querer llamar a la función ``bar()`` desde ``foo()`` se ha usado el keyword ``this`` sin necesidad alguna, ya que simplemente se podia haber llamado a la función invocandola como ``bar()`` puesto que esta en contexto global. Al estar en contexto global ambas funciones con ``this.bar()`` se dispara la función pero cuando se trata de sacar el ``console.log( this.a )`` nos devuelve undefined.

Se ha querido tener acceso a la variable ``a`` de la función foo, pero en realidad se ha accedido a ``window.a`` que es undefined. 

```javascript
function foo() {
	var a = 2;
	this.bar();
}

function bar() {
	console.log( this.a );
}

window.a = " Se accede aquí"

foo(); // "Se accede aquí"
```

