### Promesas

Casi todos los programas medianamente largos o complejos en algun momento de su vida necesitan realizar ejecuciones que se mantenienen a la espera para mas tarde seguir ejecutando el código.

Es en estos casos donde entra en juego la asincronía. Se pueden ver este tipo de casos cuando esperamos un user input o cuando hacemos una petición a una base de datos y nuestra ejecución no puede continuar hasta resolver la petición.

Mucha gente utiliza recurso de utilizar callback functions, el cual en algunos casos puede sacar del apuro. Los problemas aparecen cuando tenemos que anidar sucesivos callbacks para realizar nuestra ejecución, dando pie a el famoso *callback hell*.

##### ejemplo estructura callback hell (callbackhell.com)

```javascript

fs.readdir(source, function (err, files) {
    if (err) {
        console.log('Error finding files: ' + err)
    } else {
        files.forEach(function (filename, fileIndex) {
            console.log(filename)
            gm(source + filename).size(function (err, values) {
                if (err) {
                    console.log('Error identifying file size: ' + err)
                } else {
                    console.log(filename + ' : ' + values)
                    aspect = (values.width / values.height)
                    widths.forEach(function (width, widthIndex)
                    {
                        height = Math.round(width / aspect)
                        console.log('resizing ' + filename + 'to ' + height + 'x' + height)
                        this.resize(width, height).write(dest + 'w' + width + '_' + filename, function(err) {
                        if (err) console.log('Error writing file: ' + err)
                        })
                    }.bind(this))
                }
            })
        })
    }
})

```

En el ejemplo anterior vemos como nuestro código va tomando forma de piramide. Cuando tratamos  nuestro código de manera sincrona, tratando que se ejecute siguiendo el orden de afuera hacia dentro acaba ocurriendo lo siguiente. Otros lenguajes se comportan de manera sincrona y el código no continua ejecutandose hasta que no obtenemos el resultado de la linea que se está ejecutando.


Puede resultar confuso como se comporta el callback, vamos a analizar el siguiente ejemplo.


##### ejemplo callback (callbackhell.com)

```javascript

downloadPhoto('http://coolcats.com/cat.gif', handlePhoto)

function handlePhoto (error, photo) {
    if (error) console.error('Download error!', error)
    else console.log('Download finished', photo)
}

console.log('Download started')

```

Primero se ejecutará la función ``downloadPhoto()`` y tratará de descargar la dirección que le pasamos como primer parámetro. Al ser una función asincrona, el programa continua ejecutando la siguiente linea aunque el gif no haya terminado de descargarse. En el momento que el gif es descargado se ejecuta la función callback, en este caso ``handlePhoto()``.

    1- Descarga foto
    2- // 'Download started' 
    3- handlePhoto()





