### Arrow functions

Las arrow functions son una nueva manera de declarar las funciones en ES6, además de la syntaxis existen ciertas diferencias con las funciones anteriores a ES6.

##### Antes 

```javascript
    function foo(){
        // Contenido
    }
```

#### ES6

```javascript
    const foo = ()=>{

    }
```
Si la función tiene un parámetro, los paréntesis son opcionales. Si no tiene o tiene más de uno, deben ponerse.

```javascript
// Sin parámetros
var af = () => {};  
// Con un parámetro
var af = a => {};  
// Con varios parámetros
var af = (a, b, c) => {}; 
```

Con las espresiones ocurre algo similar. Si solo es una expresión se pueden omitir las llaves, pero si son varias, deben ponerse:

```javascript
var suma = (a, b) => a + b;

var suma = (a, b) => {  
  var resultado = a + b;
  console.log('El resultado de la suma es', resultado)
  return resultado;
}
```

### ventajas

Algunas de los principales beneficios:

1. Es mucho mas conciso.
2. Tiene el ``return`` implicito. Lo cual permite generar funciones en una linea y generar codigo muy limpio.
3. Cambio de comportamiento de ``this``. Ahora ``this`` se va a referir al contexto donde la función haya sido declarada
4. Las funciones declaradas son funciones anonimas, no tienen nombre, aunque podemos declararla dentro de una constante de la siguiente manera. 

```javascript 
const sumar = (a,b) => a + b ;
sumar(4,5); // 9
```

### ejemplos prácticos 

#### ejemplo 1

Un caso muy común es tener los datos en formato array y querer pasarlos a un objeto. 

```javascript
    const nombres = ["pepe","alvaro","juan","antonio","mikaela"];

    const edad = 15;

    let personas = nombres.map(nombre => ({ name: nombre , age: edad }))
    
    console.log(personas);
```

##### resultado del log


| index         | name          | age  |
| ------------- |:-------------:| -----:|
| 0             | pepe          |  15   |
| 1             | alvaro        |  15   |
| 2             | juan          |  15   |
| 3             | antonio       |  15   |
| 4             | mikaela       |  15   |


Se puede apreciar que al hacer el map queremos devolver un objeto, para ello debemos poner entre parentesis el objeto, ya que por defecto lo que retorna el map debe ir entre llaves, al igual que la estructura de un objeto. 

```javascript
tal.map((cual)=>{ esto: cual, ese: pascual})  : INCORRECTO 
____________________________________________
tal.map((cual)=>{{ esto: cual, ese: pascual}})  INCORRECTO
____________________________________________
tal.map((cual)=>({ esto: cual, ese: pascual})) CORRECTO
```

#### ejemplo 2

Disponemos de un array con valores y queremos sacar los valores superiores a "x" cantidad.

```javascript
    const cantidades = [25,60,51,30,70,80];

    let mayoresDeCincuenta = cantidades.filter(cantidad => cantidad > 50);

    console.log(mayoresDeCincuenta); // [60,51,70,80]
```

### Arrow functions y ``this``

Vamos a entender un poco como trabaja el lenguaje JavaScript para entender como funciona el keyword ``this``. El interprete de JavaScript lee linea a linea, el entorno  en el que se ejecuta es conocido como contexto de ejecución. 

El contexto de ejecución que se encuentra arriba del todo en la pila de ejecución será el que se está ejecutando; y el keyword ``this`` hará referencia al contexto en que se ejecuta.

Por defecto el keyword ``this`` en el browser a nivel global hace referencia al objeto *window* y si es ejecutado en NodeJs a el objeto *global*. 


### Cuando usar arrow function con ``this`` y cuando no.

Vamos a ver con dos ejempos como el ``this`` actua distinto en función de como declaremos la función.

##### Declarandolo sin arrow function

```javascript
    var persona = {
        
        name: "paco",
        altura: 150,
        sayHello: function(){ 
         console.log(`Me llamo ${this.name}`)}

    }

persona.sayHello(); // Me llamo paco
```

En este ejemplo se ve claro el proposito y se ve que en ``this`` corresponde con el contexto que es el objeto persona.

Pero veremos como en este caso realizandolo con arrow function no obtenemos el resultado esperado.


```javascript
    var persona = {
        
        name: "paco",
        altura: 150,
        sayHello: ()=>{ 
         console.log(`Me llamo ${this.name}`)
         
         }

    }

persona.sayHello(); //  Me llamo
```

El contexto en este caso no es el objeto persona, el arrow function se comporta distinto a una declaración de funcion anonima corriente. En las arrow function el ``this`` hace referencia al contexto que esté declarado por encima de este, la declaración de un arrow function no genera contexto a diferencia de una funcion anonima corriente. En el ejemplo anterior el ``this`` estaría haciendo refencia al contexto ``window``.

##### function() vs ()=> . Console.log(this) 

```javascript
    
    var persona = {
        name: "paco",
        altura: 150,
        sayHello: function(){ 
   	        let b =  ()=> console.log(this)
            b()
        }
    }

persona.sayHello(); // Devuelve el objeto persona
```

```javascript
    
    var persona = {
        name: "paco",
        altura: 150,
        sayHello: ()=>{ 
   	        let b =  ()=> console.log(this)
            b()
        }
    }

persona.sayHello(); // Devuelve el objeto window
```







