# Template strings/Template literals

Cuando queremos pasar una variable dentro de una string suele ser un trabajo algo engorroso si no es realizado con una template string.

##### ejemplo

```javascript
    let nombre = "Mikel";
    let apellido = "Tejero";
    let años = "30";
    console.log("Me llamo " + nombre + " " + apellido + " y tengo " + años + " años");

    //Me llamo Mikel Tejero y tengo 30 años
```

Como vemos en el ejemplo cada vez que queremos intercarlar una variable tenemos que cerrar las comillas del string y concatenar la variable con el ``+``.

A continuación el mismo ejemplo con template strings.

##### ejemplo con template strings

```javascript

    let nombre = "Mikel";
    let apellido = "Tejero";
    let años = "30";
    console.log(`Me llamo ${nombre} ${apellido} y tengo ${años} años`);

    //Me llamo Mikel Tejero y tengo 30 años
```

Por lo tanto vemos el que es bastante mas claro con los template string. El string tiene que ir entre comillas invertidas --->` `` ` Y las variables dentro de llaves con el simbolo del dolar por delante. ``${var}``.  

## Generando fragmentos de HTML con template literals

Con los template strings podemos pasar código html en varias lineas a una variable, de modo que podemos mantener la estructura de un html tabulada y pasarle variables. 

##### Ejemplo de un template string con código html utilizando propiedades de un objeto

```javascript
    let film = {
        name: "Pelicula maravillosa",
        year: " 1987 ",
        director: " Al patxi no "
    }

    let markup = `
        <div class="film">
            <h2>
                ${film.name}
            </h2>
            <p class="year">${film.year}</p>
            <p class="director">${film.director}</p>
        </div>
    `;
```

Como ver el código HTML mantiene su estructura original y se ve muy limpio. Teniendo nuestro código en una variable lo podemos renderizar en el body de nuestra página del siguiento modo,

```javascript
    document.body.innerHTML = markup;
```

Si hacemos un ``console.log(markup)``  la consola devuelve el string respetando su estructura e indentación.

##### Ejemplos de uso variados .map()

```javascript
    const disc = [
        {song: "track1", length: "120s"},
        {song: "track2", length: "140s"},
        {song: "track3", length: "89s"},
        {song: "track4", length: "1230s"}
    ]

    let markup = `
        <div>
            <ul>
                ${ disc.map((item) => 
                `<li> 
                    Canción ${item.song}, duración ${item.length}
                </li>`)
                .join(" ")}
            </ul>
        </div>
    `;

```

De esta manera nos generará un ``<li><li/>`` por cada item que se mapea del array ``disc``.Por ultimo necesitamos utilizar el metodo ``.join(" ")`` para eliminar las comas que devuelve ``.map()`` entre items.

##### Resultado

```html
        <div>
            <ul>
                <li> 
                    Canción track1, duración 120s
                </li> <li> 
                    Canción track2, duración 140s
                </li> <li> 
                    Canción track3, duración 89s
                </li> <li> 
                    Canción track4, duración 1230s
                </li>
            </ul>
        </div>
```

#### Tagged template strings

Las tagged template strings son funciones que son llamadas  con template literals como parametros.

##### ejemplo

Declarando la función :
```javascript 
    function miTaggedTemplateLiteral(strings, ...values) {
        return console.log(strings, ...values);
    }
```

Hemos declarado una función que recoge un array de strings y un array de valores y los devuelve por consola.

Ahora vemos como se llama a esta función a modo de tagged template string.

```javascript

    let valor1 = 2;
    let valor1 = 3:
    let valor1 = 4;

    miTaggedTemplateLiteral`Esto es un template al que le paso varios valores como este ${valor1} o este ${valor2} o este otro ${valor3}`

    // "esto es un template al que le paso varios valores como este", "o este", "o este otro", 2, 3, 4 
```

Como hemos visto al llamar a la función pasandole un template string como parametro, tomada cada string intercalada con las variables como el primer argumento y como siguientes argumentos toma cada variable del template string. Por lo tango el primer argumento es un array de strings y los siguientes cada variable dentro del template string.

El tagged template literal declarado anteriormente no tiene demasiada utilidad, pero de esta manera podemos generar una tagged template que genere plantillas dinamicamente lo cual nos puede resultar de mucha utilidad como podemos ver en el siguiente ejemplo.

```javascript 
    function generaTemplate (strings, ...keys) {
        return function(data) {
            let temp = strings.slice();
            keys.forEach((key, i) => {
                temp[i] = `${temp[i]} ${data[key]}`;
            });
            return temp.join('');
        } 
    }

    const producto = {
        nombre: "Google Pixel L",
        imagen: "http://example.com/miImagen.png",
        precio: 699,
    };



    const producto2 = {
        nombre: "willy",
        imagen: "http://cp.com/imagen.jpg",
        precio: 50
    }


    const ProductTemplate = generaTemplate`<article>
        <h1>${'nombre'}</h1>
        <img src=${'imagen'} />
        <span>${'precio'} € </span>
    </article>`(producto2);


    //"<article>
    //<h1> willy</h1>
    //<img src= http://cp.com/imagen.jpg />
    //<span> 50 € </span>
    //</article>"

```

De esta manera la función concatena las strings del template literal con las propiedades del objeto que le pasamos.

