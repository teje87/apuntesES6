### Otros metodos de ``Array``

### ``.from()`` y ``.off()``

Son metodos que no se encuentran en el prototype. Es decir no podemos invocar el metodo directamente desde el array.

##### ejemplo incorrecto

```javascript

const ordenadores = document.querySelectorAll("p")

const arrayOrdenadores = ordenadores.from()
```

#### ejemplo correcto

```javascript

const ordenadores = document.querySelectorAll("p")]

const arrayOrdenadores = Array.from(ordenadores)
```

Al seleccionar todos los parrafos del documento, nos devuelve un elemento con Prototype ``NodeList``. El problema es que si intentamos mapear este elemento nos devuelve un error. Es aqui cuando el ``from()`` entra en juego. Nuestro NodeList es un elemento "array-ish", quiere decir parecido a un array pero sin serlo; el metodo ``from()`` devolvera de este elemento un array, el cual podremos tratas con todos los metodos de array.

Otro ejemplo array-ish es ``arguments``.

```javascript

    function test(){
        console.log(arguments)
    }

    test(1,2,3,4) // [1,2,3,4] pero de Prototype Arguments
```

El metodo ``.off()`` es bastante directo, los argumentos recibidos son devueltos en un array.

```javascript
    const values = Array.of(12,13,14)

    console.log(values) // [12,13,14] 
```

___

### ``.find()`` y ``.findIndex()`` 

El metodo ``.find()`` recorre el array y cuando encuentra una entrada con los criterios indicados devuelve esta entrada. Funciona como el metodo ``.filter()``, a diferencia que filter devuelve en un array todas las entradas que coinciden con el criterio y ``.find()`` solo devuelve una entrada.

```javascript 

const coches = [
    {marca: "Seat" , modelo: "Ibiza" , año: 1998 },
    {marca: "Seat" , modelo: "Leon" , año: 2010 },
    {marca: "Audi" , modelo: "A3" , año: 2015 },
    {marca: "Audi",  modelo: "A6" , año: 2018 }
]

const selected = coches.find((coche)=> coche.modelo == "A6")

console.log(selected) // {marca: "Audi", modelo: "A6", año: 2018}

```

``.findIndex()`` se aplica de la misma manera pero devuelve el index de la entrada que coincide con el criterio.

```javascript
const coches = [
    {marca: "Seat" , modelo: "Ibiza" , año: 1998 },
    {marca: "Seat" , modelo: "Leon" , año: 2010 },
    {marca: "Audi" , modelo: "A3" , año: 2015 },
    {marca: "Audi",  modelo: "A6" , año: 2018 }
]

const selected = coches.findIndex((coche)=> coche.modelo == "A6")

console.log(selected) // 3
```

___

### ``.some()`` y ``.every()``

Ambos metodos no son parte de ES6 pero son metodos que no son comunmente utilizados pero no por ello menos utiles. ``.some()`` recorre el array en busca de alguna entrada que cumpla el criterio indicado, si al menos una de ellas lo cumple, devolverá ``true``.

```javascript

    const edades = [15,13,17,18];

    console.log(edades.some(edad=> edad >= 18)) // true


```

El metodo ``.every()`` recorre todo el array y si todas las entradas recorridas cumplen el criterio devuelve ``true``.

```javascript

    const edades = [15,13,17,18];

    console.log(edades.every(edad=> edad >= 10)) // true

```
